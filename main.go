package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"richardmace.co.uk/rnagent/agent"
)

const (
	cAppVersion    string = "0.05"
	cAppName       string = "RNAgent"
	cAppConfigFile string = "rnagent.conf"

	cUnknown string = "Unknown"
)

type args struct {
	createCSV   *bool
	dumpLocally *bool
	csvFilename *string
	folderStore *string
	ipAddress   *string
	ownerEmail  *string
	portNumber  *int
}

type ServerResponse int

const (
	NotRequired        ServerResponse = 0
	NoUser             ServerResponse = 1
	AgentAlreadyExists ServerResponse = 2
	MalformedRequest   ServerResponse = 4
	AgentCreated       ServerResponse = 8
	NoServerError      ServerResponse = 9
)

func main() {
	greetUser()

	exPath := getPath()
	args := getArgs(exPath)

	initDecisionTree(args)
}

func addAgentToServer(as *agent.AgentStruct, args *args) ServerResponse {
	requestBody, err := json.Marshal(*as)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Sending Agent data to server...")
	resp, err := http.Post(createEndpoint(args, "/agent/create"), "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	if string(body) == "Unable to find user" {
		return NoUser
	}

	if strings.Contains(string(body), "Agent created") {
		return AgentCreated
	}

	return NoServerError
}

func getArgs(exPath string) *args {
	args := &args{}

	args.createCSV = flag.Bool("createcsv", false, "Will create a CSV file containing all of the .json files in the specified folder")
	args.dumpLocally = flag.Bool("hwd", false, "Will create a .json file containing all of the hardware of the host")
	args.csvFilename = flag.String("csvfile", "output.csv", "Name of CSV file to be created")
	args.folderStore = flag.String("folder", exPath, "The folder where files will be saved")
	args.ipAddress = flag.String("ipaddress", "127.0.0.1", "IP Address of the RoCKNet Server")
	args.ownerEmail = flag.String("owner", "", "Email address of the owner of this agent")
	args.portNumber = flag.Int("port", 4000, "Port number")
	flag.Parse()

	return args
}

func initDecisionTree(args *args) {
	confFolder := addTrailingSlash(os.Getenv("ALLUSERSPROFILE")) + "RoCKNet"

	var conf configStruct
	conf, err := getConfigStruct(confFolder)
	if err != nil {
		log.Fatal(err)
	}
	if conf.guid == "" {
		// We've not been added on the server, so lets get that sorted
	}

	a := agentInit()
	a.GetHardwareSnapshot()

	if *args.dumpLocally {
		err := writeToFile(a, args)
		if err != nil {
			log.Fatal(err)
		}
	}

	if *args.ownerEmail != "" {
		if !validateEmail(*args.ownerEmail) {
			log.Fatal("The owner flag must contain a valid email address")
		}
		a.OwnerEmail = *args.ownerEmail
		res := sendHardwareJSONToServer(a, args)

		if res == NoUser {
			log.Fatal("No user was found associated with this email, and so the agent could \nnot be created. Please register with RoCKNet Admin")
		}

		if res == AgentCreated {
			log.Println("Agent created. The programmers need to write more things now.")
		}
	}
}

func greetUser() {
	log.Println("RoCKNet Agent v" + cAppVersion)
	log.Println("For help, run rnagent -h")
}

func agentInit() *agent.AgentStruct {
	a := &agent.AgentStruct{}
	return a
}

func getPath() string {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	return exPath
}

func writeToFile(a *agent.AgentStruct, args *args) error {
	jssb, _ := json.MarshalIndent(a, "", "  ")
	sFile := a.AgentHardware.HostName

	f, err := os.Create(*args.folderStore + "\\" + sFile + ".json")
	if err != nil {
		return err
	}

	log.Println("Saving hardware snapshot to file " + f.Name())
	_, err = f.WriteString(string(jssb))
	err = f.Close()
	if err != nil {
		return err
	}

	if *args.createCSV == true {
		err = writeToCSV(args)
		if err != nil {
			return err
		}
	}

	return nil
}

func writeToCSV(args *args) error {
	files, err := ioutil.ReadDir(*args.folderStore)
	if err != nil {
		return err
	}

	rows := [][]string{
		{"Host", "Make", "Model", "OS", "Memory", "Hard disk", "CPU"},
	}

	for _, f := range files {
		if strings.Contains(f.Name(), ".json") {
			data := agent.AgentStruct{}
			jsonf, err := ioutil.ReadFile(*args.folderStore + "\\" + f.Name())
			if err != nil {
				return err
			}

			err = json.Unmarshal([]byte(jsonf), &data)
			if err != nil {
				return err
			}

			totalMem := strconv.FormatInt(data.TotalMemory, 10)
			row := []string{
				data.HostName,
				data.Manufacturer,
				data.Model,
				data.OSVersion,
				totalMem,
				data.Disks[0].SizeStr,
				data.CPUName,
			}
			rows = append(rows, row)
		}
	}

	csvfile, err := os.Create(*args.folderStore + "\\" + *args.csvFilename)
	if err != nil {
		return err
	}
	defer csvfile.Close()

	csvWriter := csv.NewWriter(csvfile)
	err = csvWriter.WriteAll(rows)
	if err != nil {
		return err
	}

	log.Println("Created CSV file " + csvfile.Name())
	return nil
}

func validateEmail(ownerEmail string) bool {
	valid := strings.Contains(ownerEmail, "@")
	present := len(ownerEmail) != 0
	return valid && present
}

func createEndpoint(args *args, path string) string {
	uri := "//" + *args.ipAddress + ":" + strconv.Itoa(*args.portNumber)
	myURL, err := url.Parse(uri)
	if err != nil {
		log.Fatal(err)
	}

	return "http://" + myURL.Host + path
}

func sendHardwareJSONToServer(as *agent.AgentStruct, args *args) ServerResponse {
	requestBody, err := json.Marshal(*as)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Sending Agent data to server...")
	resp, err := http.Post(createEndpoint(args, "/agent/create"), "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	if string(body) == "Unable to find user" {
		return NoUser
	}

	if strings.Contains(string(body), "Agent created") {
		return AgentCreated
	}

	return NoServerError
}
