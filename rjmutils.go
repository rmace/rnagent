package main

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

func addToLog(logFile, txt string) error {
	f, err := os.OpenFile(logFile,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	logger := log.New(f, "", log.LstdFlags)
	logger.Println(txt)

	return nil
}

func addTrailingSlash(filePath string) string {
	if filePath == "" {
		return ""
	}
	
	var lastChar = filePath[len(filePath)-1:]
	if isWindows() {
		if lastChar == "\\" {
			return filePath
		} else {
			return filePath + "\\"
		}
	}
	if isLinux() {
		if lastChar == "/" {
			return filePath
		} else {
			return filePath + "/"
		}
	}
	return ""
}

func compressFilesInDir(dir, filespec string, removeSF bool) (compressedFile string, err error) {
	files, err := ioutil.ReadDir(dir)
	var fps []string
	if err != nil {
		return "", nil
	}

	for _, f := range files {
		if filepath.Ext(f.Name()) == (filepath.Ext(filespec)) {
			fmt.Println(f.Name())
			fps = append(fps, f.Name())
		}
	}
	t := time.Now()
	var tarfile string = t.Format("2006-01-02") + ".tar"
	CreateTarball("./"+tarfile, fps)
	if removeSF {
		for _, fs := range fps {
			err = os.Remove(fs)
			if err != nil {
				return "", nil
			}
		}
	}
	gFile, err := gzipit(dir+tarfile, dir, true)
	if err != nil {
		return "", nil
	}
	if removeSF {
		err = os.Remove(dir + tarfile)
		if err != nil {
			return "", nil
		}
	}

	return dir + gFile, nil
}

func CopyFile(srcFile, destFile string) error {
	// Open original file
	originalFile, err := os.Open(srcFile)
	if err != nil {
		return err
	}
	defer originalFile.Close()

	// Create new file
	newFile, err := os.Create(destFile)
	if err != nil {
		return err
	}
	defer newFile.Close()

	// Copy the bytes to destination from source
	bytesWritten, err := io.Copy(newFile, originalFile)
	if err != nil {
		return err
	}
	log.Printf("Copied %d bytes.", bytesWritten)

	// Commit the file contents
	// Flushes memory to disk
	err = newFile.Sync()
	if err != nil {
		return err
	}

	return nil
}

func DeleteFile(file string) error {
	err := os.Remove(file)
	if err != nil {
		return err
	}
	return nil
}

func getUserHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	} else if runtime.GOOS == "linux" {
		home := os.Getenv("XDG_CONFIG_HOME")
		if home != "" {
			return home
		}
	}
	return os.Getenv("HOME")
}

func gzipit(sourceFile, targetDir string, deleteOrig bool) (gzFile string, err error) {
	log.Print("GZipping file " + sourceFile + "...")
	reader, err := os.Open(sourceFile)
	if err != nil {
		return "", err
	}

	filename := filepath.Base(sourceFile)
	var targetFile string = filepath.Join(targetDir, fmt.Sprintf("%s.gz", filename))
	writer, err := os.Create(targetFile)
	if err != nil {
		return "", err
	}
	defer writer.Close()

	archiver := gzip.NewWriter(writer)
	archiver.Name = filename
	defer archiver.Close()

	_, err = io.Copy(archiver, reader)
	if err != nil {
		return "", err
	}
	if deleteOrig {
		log.Print("Removing source file " + sourceFile + "...")
		err = os.Remove(sourceFile)
		if err != nil {
			return "", err
		}
	}

	return targetFile, err
}

func isWindows() bool {
	if runtime.GOOS == "windows" {
		return true
	} else {
		return false
	}
}

func isLinux() bool {
	if runtime.GOOS == "linux" {
		return true
	} else {
		return false
	}
}

func CreateTarball(tarballFilePath string, filePaths []string) error {
	file, err := os.Create(tarballFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not create tarball file '%s', got error '%s'", tarballFilePath, err.Error()))
	}
	defer file.Close()

	gzipWriter := gzip.NewWriter(file)
	defer gzipWriter.Close()

	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	for _, filePath := range filePaths {
		err := addFileToTarWriter(filePath, tarWriter)
		if err != nil {
			return errors.New(fmt.Sprintf("Could not add file '%s', to tarball, got error '%s'", filePath, err.Error()))
		}
	}

	return nil
}

// Private methods

func addFileToTarWriter(filePath string, tarWriter *tar.Writer) error {
	file, err := os.Open(filePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not open file '%s', got error '%s'", filePath, err.Error()))
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return errors.New(fmt.Sprintf("Could not get stat for file '%s', got error '%s'", filePath, err.Error()))
	}

	header := &tar.Header{
		Name:    filePath,
		Size:    stat.Size(),
		Mode:    int64(stat.Mode()),
		ModTime: stat.ModTime(),
	}

	err = tarWriter.WriteHeader(header)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not write header for file '%s', got error '%s'", filePath, err.Error()))
	}

	_, err = io.Copy(tarWriter, file)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not copy the file '%s' data to the tarball, got error '%s'", filePath, err.Error()))
	}

	return nil
}

// Unzip will decompress a zip archive, moving all files and folders
// within the zip file (parameter 1) to an output directory (parameter 2).
func Unzip(src string, dest string) ([]string, error) {

	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return filenames, err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return filenames, fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return filenames, err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return filenames, err
		}

		rc, err := f.Open()
		if err != nil {
			return filenames, err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return filenames, err
		}
	}
	return filenames, nil
}

func WriteTextToFile(fileName, text string) error {
	// Open a new file for writing only
	file, err := os.OpenFile(
		fileName,
		os.O_WRONLY|os.O_APPEND|os.O_CREATE,
		0666,
	)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Write bytes to file
	byteSlice := []byte(text + "\n")
	_, err = file.Write(byteSlice)
	if err != nil {
		log.Fatal(err)
	}
	//log.Printf("Wrote %d bytes.\n", bytesWritten)

	return nil
}
