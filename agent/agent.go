package agent

import (
	"github.com/StackExchange/wmi"
	"log"
	"math"
	"os"
	"strconv"
	"time"
)

type AgentStruct struct {
	Id      int
	Created time.Time `orm:"auto_now_add;type(timestamp)"`
	Updated time.Time `orm:"auto_now;type(timestamp)"`
	Deleted bool
	KnownAs string `orm:"size(25)"`
	AgentHardware
	Name       string `valid:"Required"; orm:"size(25)"`
	OwnerEmail string `orm:"size(100)"`
	UserId     int
	Version    string `orm:"size(10)"`
}

// TODO: needed?
type TestingJson struct {
	Id int `json:"Id"`
}

type Win32_Bios struct {
	SerialNumber string
}

type Win32_ComputerSystem struct {
	Domain                    string
	Manufacturer              string
	Model                     string
	NumberOfLogicalProcessors int32
	NumberOfProcessors        int32
	PartOfDomain              bool
	//Processor    string
	SystemSKUNumber string
	UserName        string
}

type Win32_LogicalDisk struct {
	Caption            string
	FreeSpace          int64
	MediaType          int32
	Name               string
	Size               int64
	VolumeName         string
	VolumeSerialNumber string
}

type Win32_NetworkAdapterConfiguration struct {
	Caption              string
	Description          string
	DefaultIPGateway     []string
	DHCPEnabled          bool
	DHCPServer           string
	DNSServerSearchOrder []string
	IPAddress            []string
	IPSubnet             []string
	MACAddress           string
	ServiceName          string
}

type Win32_OperatingSystem struct {
	//OperatingSystemSKU int32
	BuildNumber string
	SuiteMask   int32
	Version     string
}

type Win32_PhysicalMemory struct {
	Capacity      int64
	DeviceLocator string
	Manufacturer  string
	MemoryType    int16
}

type Win32_Processor struct {
	Name string
}

type PhysicalMemory struct {
	Capacity      int64
	DeviceLocator string
	Manufacturer  string
	MemoryType    int16
}

type Disk struct {
	Caption            string
	FreeSpace          int64
	FreeSpaceStr       string
	MediaType          int32
	Name               string
	PercentFree        int64
	Size               int64
	SizeStr            string
	VolumeName         string
	VolumeSerialNumber string
}

type AgentHardware struct {
	LastUpdated               time.Time
	HostName                  string
	Domain                    string
	OSVersion                 string
	Manufacturer              string
	Model                     string
	SerialNumber              string
	Disks                     []Disk
	NetworkAdapters           []NetworkAdapter
	PhysicalMemorys           []PhysicalMemory
	TotalMemory               int64
	CPUName                   string
	NumberOfProcessors        int32
	NumberOfLogicalProcessors int32
	UserName                  string
}

type NetworkAdapter struct {
	Name string
	//Description    string
	DHCPEnabled    bool
	IPAddress      []string
	MACAddress     string "Unknown"
	SubnetMask     []string
	DefaultGateway []string
	DHCPServer     string
	DNSServers     []string
	//DNSServers     []NetworkAdapterDNSServer
}

type NetworkAdapterDNSServer struct {
	IPAddress string
}
type NetworkAdapters struct {
	NetworkAdapter []NetworkAdapter
}

func round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

func (a *AgentStruct) GetBIOSInfo() {
	var binfo []Win32_Bios

	q := wmi.CreateQuery(&binfo, "")
	err := wmi.Query(q, &binfo)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range binfo {
		a.AgentHardware.SerialNumber = v.SerialNumber
	}
}

func (a *AgentStruct) GetComputerSystem() {
	var csys []Win32_ComputerSystem

	sHost, err := os.Hostname()
	if err != nil {
		a.AgentHardware.HostName = "Unknown"
	} else {
		a.AgentHardware.HostName = sHost
	}

	q := wmi.CreateQuery(&csys, "")
	err = wmi.Query(q, &csys)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range csys {
		a.AgentHardware.Domain = v.Domain
		a.AgentHardware.Manufacturer = v.Manufacturer
		a.AgentHardware.Model = v.Model
		a.AgentHardware.NumberOfProcessors = v.NumberOfProcessors
		a.AgentHardware.NumberOfLogicalProcessors = v.NumberOfLogicalProcessors
		a.AgentHardware.UserName = v.UserName
	}
}

func (a *AgentStruct) GetLocalDiskSnapshot() {
	var dst []Win32_LogicalDisk

	q := wmi.CreateQuery(&dst, "")
	err := wmi.Query(q, &dst)
	if err != nil {
		log.Fatal(err)
	}

	disks := []Disk{}
	for _, v := range dst {
		if v.MediaType == 12 {
			var dsk Disk
			var kb, mb, gb int64

			kb = v.FreeSpace / 1024
			mb = kb / 1024
			gb = mb / 1024
			dsk.FreeSpace = gb
			dsk.Name = v.Name
			dsk.Caption = v.Caption

			kb = v.Size / 1024
			mb = kb / 1024
			gb = mb / 1024
			dsk.Size = gb
			dsk.VolumeName = v.VolumeName
			dsk.VolumeSerialNumber = v.VolumeSerialNumber

			baseSize := math.Log(float64(v.Size)) / math.Log(1024)
			baseFreeSpace := math.Log(float64(v.FreeSpace)) / math.Log(1024)
			getSize := round(math.Pow(1024, baseSize-math.Floor(baseSize)), .5, 0)
			getFreeSpace := round(math.Pow(1024, baseFreeSpace-math.Floor(baseFreeSpace)), .5, 2)

			suffixes := [5]string{"B", "KB", "MB", "GB", "TB"}
			getSuffixSize := suffixes[int(math.Floor(baseSize))]
			getSuffixFreeSpace := suffixes[int(math.Floor(baseFreeSpace))]

			dsk.SizeStr = strconv.FormatFloat(getSize, 'f', -1, 64) + " " + string(getSuffixSize)
			dsk.FreeSpaceStr = strconv.FormatFloat(getFreeSpace, 'f', -1, 64) + " " + string(getSuffixFreeSpace)

			var percentFree float64
			percentFree = (float64(v.FreeSpace) / float64(v.Size)) * 100
			dsk.PercentFree = int64(percentFree)

			disks = append(disks, dsk)
		}
	}

	a.AgentHardware.Disks = disks
}

func (a *AgentStruct) GetNetworkSnapshot() {
	adapters := []NetworkAdapter{}
	var nac []Win32_NetworkAdapterConfiguration

	q := wmi.CreateQuery(&nac, "")
	err := wmi.Query(q, &nac)
	if err != nil {
		log.Fatal(err)
	}
	for _, v := range nac {
		na := NetworkAdapter{}
		//na.Name = v.Caption
		na.Name = v.Description
		na.DefaultGateway = v.DefaultIPGateway
		na.DHCPEnabled = v.DHCPEnabled
		na.DHCPServer = v.DHCPServer
		na.DNSServers = v.DNSServerSearchOrder
		na.IPAddress = v.IPAddress
		na.MACAddress = v.MACAddress
		na.SubnetMask = v.IPSubnet
		adapters = append(adapters, na)

	}
	a.AgentHardware.NetworkAdapters = adapters
}

func (a *AgentStruct) GetOperatingSystem() {
	var os []Win32_OperatingSystem

	q := wmi.CreateQuery(&os, "")
	err := wmi.Query(q, &os)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range os {
		a.AgentHardware.OSVersion = mapVersion(v.Version)
	}
}

func mapVersion(v string) string {
	vMap := map[string]string{
		"10.0.10240": "Windows 10",
		"10.0.10586": "Windows 10, version 1511",
		"10.0.14393": "Windows 10, version 1607",
		"10.0.15063": "Windows 10, version 1703",
		"10.0.16299": "Windows 10, version 1709",
		"10.0.17134": "Windows 10, version 1803",
		"10.0.17763": "Windows 10, version 1809",
		"10.0.18362": "Windows 10 version 1903",
	}

	longform := vMap[v]
	if longform == "" {
		return v
	}

	return vMap[v]
}

func (a *AgentStruct) GetHardwareSnapshot() {
	a.AgentHardware.LastUpdated = time.Now()
	a.GetProcessorInfo()
	a.GetBIOSInfo()
	a.GetComputerSystem()
	a.GetLocalDiskSnapshot()
	a.GetPhysicalMemory()
	a.GetNetworkSnapshot()
	a.GetOperatingSystem()
}

func (a *AgentStruct) GetPhysicalMemory() {
	var dst []Win32_PhysicalMemory

	q := wmi.CreateQuery(&dst, "")
	err := wmi.Query(q, &dst)
	if err != nil {
		log.Fatal(err)
	}

	var pm PhysicalMemory
	for _, v := range dst {
		// We need to check that we are only adding the correct memory...
		var kb, mb, gb int64
		kb = v.Capacity / 1024
		mb = kb / 1024
		gb = mb / 1024
		pm.Capacity = gb
		pm.Manufacturer = v.Manufacturer

		a.AgentHardware.PhysicalMemorys = append(a.AgentHardware.PhysicalMemorys, pm)
	}

	for _, m := range a.AgentHardware.PhysicalMemorys {
		a.AgentHardware.TotalMemory = a.AgentHardware.TotalMemory + m.Capacity
	}
}

func (a *AgentStruct) GetProcessorInfo() {
	var cpuinfo []Win32_Processor

	q := wmi.CreateQuery(&cpuinfo, "")
	err := wmi.Query(q, &cpuinfo)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range cpuinfo {
		a.AgentHardware.CPUName = v.Name
	}

	for _, m := range a.AgentHardware.PhysicalMemorys {
		a.AgentHardware.TotalMemory = a.AgentHardware.TotalMemory + m.Capacity
	}
}
