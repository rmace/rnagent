module richardmace.co.uk/rnagent

go 1.12

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/spf13/viper v1.6.2 // indirect
	gopkg.in/yaml.v2 v2.2.7
)
