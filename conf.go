package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type configStruct struct {
	guid string
}

func createDefaultConfFile(confDir string) error {
	var cfig configStruct

	cfig.guid = cUnknown

	jssb, err := json.MarshalIndent(cfig, "", "  ")
	if err != nil {
		return err
	}

	f, err := os.Create(confDir + cAppConfigFile)
	if err != nil {
		return err
	}

	log.Println("Creating default conf file " + f.Name())
	_, err = f.WriteString(string(jssb))
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func getConfigStruct(confDir string) (configStruct, error) {
	_, err := os.Stat(confDir)
	if err != nil {
		if os.IsNotExist(err) {
			// folder does not exist
			err = os.Mkdir(confDir, os.ModeDir)
			if err != nil {
				return configStruct{},err
			}
		} else {
			log.Print("error ", err)
		}
	}

	// Create the file if it doesn't already exist
	confDir = addTrailingSlash(confDir)
	if _, err := os.Stat(confDir + cAppConfigFile); os.IsNotExist(err) {
		createDefaultConfFile(confDir)
	}

	// Get all dbases that need to be backed up
	file, err := ioutil.ReadFile(confDir + cAppConfigFile)
	if err != nil {
		return configStruct{}, err
	}

	tbCf := configStruct{}

	err = json.Unmarshal([]byte(file), &tbCf)
	if err != nil {
		return configStruct{}, err
	}
	return tbCf, nil
}

func newConfStruct() configStruct {
	c := configStruct{}
	c.guid = cUnknown

	return c
}

func setConfigStruct(dir string, cs configStruct) error {
	jssb, _ := json.MarshalIndent(cs, "", "  ")
	dir = addTrailingSlash(dir)
	sFile := dir + cAppConfigFile

	f, err := os.Create(sFile)
	if err != nil {
		return err
	}

	_, err = f.WriteString(string(jssb))
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}
